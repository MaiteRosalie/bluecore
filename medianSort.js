// Write a function medianSort() that accepts an array.
// The elements of this array can be numbers or arrays of numbers.
// The function should return the initial array sorted by the numbers or the median values of the arrays of numbers

const expected = [
  { param: [3, [-2, 4, 9]], result: [3, [-2, 4, 9]] },
  // The median of [-2, 4, 9] is 4, so we should sort the array after 3.
  { param: [[12, 9, 1, 25], 4], result: [4, [12, 9, 1, 25]] },
  // The median of [12, 9, 1, 25] is 10.5, and 10.5 is greater than 4.
  {
    param: [[2.4, 0.2, 9.8], 0, [-1], [-9, -4]],
    result: [[-9, -4], [-1], 0, [2.4, 0.2, 9.8]],
  },
  // The median of [2.4, 0.2, 9.8] is 2.4.
  // The median of [-1] is -1.
  // The median of [-9, -4] is -6.5.
  // Therefore, the sorted order of these medians is [-6.5, -1, 0, 2.4]
];

const getMedian = (numbers = []) => {
  if (numbers.length === 0) return undefined;
  //sort from lower to higher
  const sorted = [...numbers].sort((a, b) => a - b);
  //get the number in the middle
  const middle = Math.floor(sorted.length / 2);

  // if array length is even return the average of the two numbers in the middle
  if (sorted.length % 2 === 0) {
    return (sorted[middle - 1] + sorted[middle]) / 2;
  }
  // if array length is odd return the number in the middle
  return sorted[middle];
};

const medianSort = (param = []) => {
  const getValue = (item) => (isNaN(item) ? getMedian(item) : item);
  //sort from lower to higher
  const newArray = [...param].sort((a, b) => getValue(a) - getValue(b));
  return newArray;
};

// check that returned values are as expected
expected.forEach(({ param }) => {
  console.log(medianSort(param));
});
