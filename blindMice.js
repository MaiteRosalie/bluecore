// Write a function blindMice() that will find out how many of your mice are walking away from the cheese.

const expected = [
  { param: "M~~M ~MM~C~MM~M~", result: 3 },
  { param: "~M~M~MC M~~M", result: 1 },
  { param: "~M   CM~~M~M", result: 2 },
];
const walkingRight = "~M";
const walkingLeft = "M~";

// remove unwanted characters
const sanitize = (str = "") => str.replace(/\s+/g, "");

const countWalking = (str = "", match) => {
  let result = 0;
  // check every two characters if it matches and increase the counter when it does
  for (let i = 0; i < str.length; i += walkingLeft.length) {
    const value = str[i] + str[i + 1];
    if (value === match) result += 1;
  }
  return result;
};

const blindMice = (str = "") => {
  // split the array at C so 0 is left and 1 is right
  const sides = sanitize(str).split("C");
  // count the mice walking to the left on the left side (away from the cheese)
  const blindOnLeft = countWalking(sides[0], walkingLeft);
  // count the mice walking to the right on the right side (away from the cheese)
  const blindOnRight = countWalking(sides[1], walkingRight);
  // Add up the mice walking away from the cheese
  return blindOnLeft + blindOnRight;
};

// check that returned values are as expe
expected.forEach(({ param }) => {
  console.log(blindMice(param));
});
