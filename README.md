<div align="center">
<div id="top"></div>
 <h3 align="center">Bluecore - Take-home test</h3>
</div>

## About The Project
This is the second stage at Bluecore's Integration Engineer position 💪  
[Here](https://www.notion.so/remotelyworks/Bluecore-Take-home-Test-bca97b48b00448dfb44ed2522df951a1) is the link to the take-home. Ideally, you have 2 hours to finish it. You will see it includes 3 exercises, do as much as you can but don't worry if you cannot finish it.

## Exercises
### 1 - Blind Mice
You just bought a delicious,  yet odorless,  cheese for your pet mice. Unfortunately some of them  are blind, so  they start walking away from this  otherwise-irresistible treat!

Write a function `blindMice()`  that will find out how many of your mice are blind. This function  should  accept a single  argument, a  string representing the mice and the cheese, and returns the number of mice that are blind. We can assume that all mice walking away from the cheese are blind, while those walking towards it are not.

Variables:
- Cheese: 'C' 
- Mice walking right: '~M' 
- Mice walking left: 'M~'

Example inputs and correct return values: 
- `M~~M ~MM~C~MM~M~`  →  3 
- `~M~M~MC M~~M` →   1 
- `~M CM~~M~M`  →   2


### 2 - Check  Inventory
You have just started working at  a  brand-new retail store. Unfortunately,  it is difficult to remember which products are in stock  due to the store’s unique  inventory system.  Write a function `checkInventory(I,R)`  that will accept 2 arguments:

Variables:
- I  –  Inventory (nested object  –  see example below)
```
{
	attribute_1:
	{
		value_1: [array of integer product IDs],
		value_2: [array of integer product IDs]
	},
	attribute_2:
	{
		value_1: [array of integer product IDs],
		value_2: [array of integer product IDs]
	}
}
```
- R  –  Request  (object)
```
{
	attribute_1: value_1,
	attribute_2: value_2
}
```
Your function should return an array of  product IDs  that match all the  attributes in the request. If there are no matches, return an empty array.  Note that there can be any number of attributes and values in the inventory object.  The request may also be a partial request (only contains some of the total attributes).

Example inputs and correct return values: 
```
I = {
	color: {  blue: [123,456,789],red: [234,567,890]  },
	size: {  small: [123,234], medium: [456,789], large: [567,890]}
}
```
- `R = {color: 'red  ', size: 'small  '} ` →  `[234]`
- `R = {color: 'red  '}` →  `[234,567,890]`
- `R = {color: 'green'}` →  `[]`


### 2 - Median Sort
Write a function `medianSort()`  that accepts one argument: an array. The elements of this array can be  either numbers or arrays of numbers. The function  should return the initial array sorted by the numbers or the median values of the arrays of numbers.

Example inputs and correct return values: 

- The median of [-2, 4, 9]  is 4, so we should sort that array after the 3.
 `medianSort([3, [-2, 4, 9]])` →  `[3, [-2, 4, 9]]`
 
 - The median of  [12, 9, 1, 25]  is 10.5, and 10.5 is greater than 4.
 `medianSort([[12, 9, 1, 25], 4])` →  `[4, [12, 9, 1, 25]]`
 
 - The median of `[2.4, 0.2, 9.8]`  is 2.4. The median of  `[-1]`  is -1. The median of `[-9, -4]`  is -6.5. Therefore, the sorted order of these medians is `[-6.5, -1, 0, 2.4]`.
 `medianSort([[2.4, 0.2, 9.8], 0, [-1], [-9, -4]])` →  `[[-9, -4], [-1], 0, [2.4, 0.2, 9.8]]`

<p align="right"><a href="#top">back to top</a></p>
