// Write a function checkInventory(I,R) that returns an array of  IDs that match all the attributes in the request. If there are no matches, return an empty array.
// Note:
// There can be any number of attributes and values in the inventory object.
// The request may also be a partial request (only contains some of the total attributes).

const I = {
  color: { blue: [123, 456, 789], red: [234, 567, 890] },
  size: { small: [123, 234], medium: [456, 789], large: [567, 890] },
};
const expected = [
  { param: { I, R: { color: "red ", size: "small " } }, result: [234] },
  { param: { I, R: { color: "red " } }, result: [234, 567, 890] },
  { param: { I, R: { color: "green" } }, result: [] },
];

// remove unwanted characters
const sanitize = (str = "") => str.replace(/\s+/g, "");

const checkInventory = ({ I, R }) => {
  let ids = [];
  let matchedCount = 0;

  // get all keys and values from the request
  for (const [k, v] of Object.entries(R)) {
    const attr = sanitize(k);
    const value = sanitize(v);
    // if the attribute exists on the inventory add it to the id's array and increase the counter
    if (I[attr] && I[attr][value] !== undefined) {
      ids = [...ids, ...I[attr][value]];
      matchedCount = matchedCount += 1;
    }
  }

  // if more than one attribute matches get the Id that comes in all of them
  if (matchedCount > 1) {
    // get repeated ids
    return ids.filter((e, i, a) => a.indexOf(e) !== i);
  }
  // if there is only one attribute return its ids
  return ids;
};

// check that returned values are as expected
expected.forEach(({ param }) => {
  console.log(checkInventory(param));
});
